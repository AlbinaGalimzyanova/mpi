#include "mpi.h"
#include <math.h>
using namespace std;

/* На процессе с номером 0 объявить и заполнить значениями матрицу a[8][8]. Создать новый тип данных для 
рассылки нечетных строк матрицы а в матрицу b[4][8], и для рассылки четных строк матрицы в матрицу c[4][8], 
используя функцию MPI_Type_vector. */

const int N = 8;
const int M = 4;

int *generate_matrix(int rank)
{
    bool is_null = false;
    int *a = new int[rank * rank];

    for (int i = 0; i < rank; i++)
    {
        for (int j = 0; j < rank; j++)
        {
            if (is_null)
                a[i * rank + j] = 0;
            else
                a[i * rank + j] = rand() % 100;
        }
    }

    return a;
}

void print_matrix(int *a, int rank)
{
    for (int i = 0; i < rank; i++)
        for (int j = 0; j < rank; j++)
            printf("%d ", a[i * rank + j]);
        printf("\n");
}

int main()
{
    int process_number, process_count;
    MPI_Init(NULL, NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &process_count);
    MPI_Comm_rank(MPI_COMM_WORLD, &process_number);

    MPI_Datatype matrix_type;
    MPI_Type_vector(4, 8, 16, MPI_INT, &matrix_type);
    MPI_Type_commit(&matrix_type);

    if (process_number == 0) {
        int *a = generate_matrix(8);
        print_matrix(a, 8);
        MPI_Send(a, 1, matrix_type, 1, 0, MPI_COMM_WORLD);
        MPI_Send(a + 8, 1, matrix_type, 2, 1, MPI_COMM_WORLD);
        printf("\n");
    }
    else if (process_number == 1) {
        int b[32];
        MPI_Recv(b, 32, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 8; j++) {
                printf("%d ", b[i * 8 + j]);
            }
            printf("\n");
        }
        printf("\n");
    }
    else if (process_number == 2) {
        int c[32];
        MPI_Recv(c, 32, MPI_INT, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 8; j++) {
                printf("%d ", c[i * 8 + j]);
            }
            printf("\n");
        }
        printf("\n");
    }

    MPI_Finalize();
}