#include "mpi.h"
#include <math.h>
using namespace std;

/* Создать новую группу из процессов с номерами 8, 3, 9, 1, 6. Используя конструктор MPI_Comm_create(…), 
создать коммуникатор группы. На нулевом процессе созданной группы объявить и заполнить числовыми значениями 
одномерный массив вещественных чисел и вывести его на экран. Используя функцию MPI_Bcast передать массив всем 
процессам созданной группы. Полученные массивы вывести на экран. Передать полученный массив из последнего 
процесса новой группы на нулевой процесс исходной группы. Выполнить программу на 10 процессах. */

// ????????????

int *generate_matrix(int rank)
{
    bool is_null = false;

    int *a = new int[rank * rank];

    for (int i = 0; i < rank; i++)
        for (int j = 0; j < rank; j++)
            a[i * rank + j] = is_null ? 0 : rand() % 100;

    return a;
}

void print_matrix(int *a, int rank)
{
    for (int i = 0; i < rank; i++)
        for (int j = 0; j < rank; j++)
            cout << a[i * rank + j] << " ";

        cout << endl;
}

int main() {
	MPI_Init(NULL, NULL);

	int current_process_id;
    MPI_Comm_rank(MPI_COMM_WORLD, &current_process_id);

    int number_of_processes;
    MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);

    int ranks[5] = {8, 3, 9, 1, 6};

    MPI_Group wgroup, group1, group2;
    MPI_Comm_group(MPI_COMM_WORLD, &wgroup);
    MPI_Group_incl(wgroup, 5, ranks, &group1);
    MPI_Group_excl(wgroup, 5, ranks, &group2);

    MPI_Comm comm1, comm2, inter_comm, local_comm;
    MPI_Comm_create(MPI_COMM_WORLD, group1, &comm1);
    MPI_Comm_create(MPI_COMM_WORLD, group2, &comm2);

    int rank1 = -1;
    int rank2 = -1;

    int remote_leader = -1;

    if (comm1 != MPI_COMM_NULL)
    {
        MPI_Comm_rank(comm1, &rank1);
    }

    if (comm2 != MPI_COMM_NULL)
    {
        MPI_Comm_rank(comm2, &rank2);
    }

    int local_leader;

    if (comm1 != MPI_COMM_NULL)
    {
        local_comm = comm1;
        remote_leader = 0;
        local_leader = 4;
    }

    if (comm2 != MPI_COMM_NULL)
    {
        local_comm = comm2;
        remote_leader = 6;
        local_leader = 0;
    }

    MPI_Intercomm_create(local_comm, local_leader, MPI_COMM_WORLD, remote_leader, 1, &inter_comm);

    if (comm1 != MPI_COMM_NULL)
    {
        double vec[10];

        if (rank1 == 0)
        {
            cout << "Vector: " << endl;

            for (int i = 0; i < 10; i++)
            {
                vec[i] = rand() % 10 + 2;

                cout << vec[i] << " ";
            }

            cout << endl;
        }

        MPI_Bcast(vec, 10, MPI_DOUBLE, 0, comm1);

        cout << "Process " << rank1 << endl;

        for (int i = 0; i < 10; i++)
            cout << vec[i] << " ";
        
        cout << endl;

        if (rank1 == 4)
            MPI_Send(vec, 10, MPI_DOUBLE, 0, 2, inter_comm);
    }

    if (rank2 == 0)
    {
        double *v = new double[10];

        MPI_Recv(v, 10, MPI_DOUBLE, MPI_ANY_SOURCE, MPI_ANY_TAG, inter_comm, MPI_STATUSES_IGNORE);

        cout << "0 process received rank2 = " << rank2 << ", process number = " << current_process_id << endl;

        for (int i = 0; i < 10; i++)
            cout << v[i] << " ";

        cout << endl;

        delete[] v;
    }

    if (comm1 != MPI_COMM_NULL)
    {
        MPI_Comm_free(&comm1);
    }

    if (comm2 != MPI_COMM_NULL)
    {
        MPI_Comm_free(&comm2);
    }

    MPI_Group_free(&group1);
    MPI_Group_free(&group2);

    MPI_Finalize();
}