#include "mpi.h"
#include <math.h>
using namespace std;

/* Создать тип данных с помощью функции MPI_Type_create_struct и выполнить рассылку строк матрицы a[8][8] 
на 4 процесса в матрицу d[2][8] по следующей схеме:
    на нулевой процесс строки 0 и 4,
    на первый процесс строки 1 и 5,
    на второй процесс строки 2 и 6,
    на третий процесс строки 3 и 7. */

const int N = 8;

struct Block {
    int matrix[2][8];
};

int main()
{
	MPI_Init(NULL, NULL);

	int current_process_id;
    MPI_Comm_rank(MPI_COMM_WORLD, &current_process_id);

    int number_of_processes;
    MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);

    MPI_Datatype matrix_type;
    int blockLengths[1] = {16};
    MPI_Datatype types[1] = {MPI_INT};
    MPI_Aint disp[1] = {0};
    MPI_Type_create_struct(1, blockLengths, disp, types, &matrix_type);
    MPI_Type_commit(&matrix_type);
    
    Block block;

    if (current_process_id == 0)
    {
        int *a = new int[N * N];

        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                a[i * N + j] = rand() % 100;

        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
                cout << a[i * N + j] << " ";

            cout << endl;
        }

        for (int i = 0; i < N; i++)
        {
            block.matrix[0][i] = a[0 * N + i];
            block.matrix[1][i] = a[4 * N + i];
        }

        MPI_Send(&block, 1, matrix_type, 0, 0, MPI_COMM_WORLD);

        for (int i = 0; i < N; i++)
        {
            block.matrix[0][i] = a[1 * N + i];
            block.matrix[1][i] = a[5 * N + i];
        }

        MPI_Send(&block, 1, matrix_type, 1, 0, MPI_COMM_WORLD);

        for (int i = 0; i < 8; i++)
        {
            block.matrix[0][i] = a[2 * N + i];
            block.matrix[1][i] = a[6 * N + i];
        }

        MPI_Send(&block, 1, matrix_type, 2, 0, MPI_COMM_WORLD);

        for (int i = 0; i < 8; i++)
        {
            block.matrix[0][i] = a[3 * N + i];
            block.matrix[1][i] = a[7 * N + i];
        }

        MPI_Send(&block, 1, matrix_type, 3, 0, MPI_COMM_WORLD);

        cout << endl;
    }

    MPI_Status status;
    
    MPI_Recv(&block, 1, matrix_type, 0, 0, MPI_COMM_WORLD, &status);

    cout << "Process " << current_process_id << endl;

    for (int i = 0; i < 2; i++)
    {
        for (int j = 0; j < N; j++)
            cout << block.matrix[i][j] << " ";
        
        cout << endl;
    }

    cout << endl;

    MPI_Finalize();
}