#include <mpi.h>
using namespace std;

/* Написать программу, вычисляющую элементы вектора z  по формуле zi=xi+yi. Векторы х, у задаются на нулевом 
процессе и равными блоками пересылаются остальным процессам, , - заданные числа. Пересылка данных 
осуществляется функцией MPI_Send. Все процессы по формуле вычисляют свои элементы массива z. Каждый процесс 
отправляет на нулевой процесс подсчитанные элементы. */

const int a = 5;
const int b = 2;

int main()
{
    MPI_Init(NULL, NULL);

	int current_process_id;
    MPI_Comm_rank(MPI_COMM_WORLD, &current_process_id);

    int number_of_processes;
    MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);

    int array_size = 12;
    int chunk_size = array_size / number_of_processes + 1;

    const int X_TAG = 0;
    const int Y_TAG = 1;
    const int Z_TAG = 2;

    if (current_process_id == 0)
    {
        int x[array_size];
        int y[array_size];
        float z[array_size];

        for (int i = 0; i < array_size; i++)
        {
            x[i] = rand() % 100;
            y[i] = rand() % 100;
        }

        int left_items_count = array_size;

        for (int process_index = 0; process_index < number_of_processes; process_index++)
        {
            int count_for_sending;

            if (left_items_count < chunk_size)
                count_for_sending = left_items_count;
            else
                count_for_sending = chunk_size;

            MPI_Send(&x[array_size - left_items_count], count_for_sending, MPI_INT, process_index, X_TAG, MPI_COMM_WORLD);
            MPI_Send(&x[array_size - left_items_count], count_for_sending, MPI_INT, process_index, Y_TAG, MPI_COMM_WORLD);
            
            left_items_count -= count_for_sending;
        }

        int recv_count = 0;

        for (int process_index = 1; process_index < number_of_processes; process_index++)
        {
            MPI_Status status;
            MPI_Probe(process_index, Z_TAG, MPI_COMM_WORLD, &status);

            int recv_array_size;
            MPI_Get_count(&status, MPI_INT, &recv_array_size);

            MPI_Recv(&z[recv_count], recv_array_size, MPI_INT, process_index, Z_TAG, MPI_COMM_WORLD, &status);

            recv_count += recv_array_size;
        }

        for (int i = 0; i < array_size; i++)
            cout << z[i] << " ";
        
        cout << endl;
    }
    else
    {
        MPI_Status status;
        MPI_Probe(0, X_TAG, MPI_COMM_WORLD, &status);

        int recv_array_size;
        MPI_Get_count(&status, MPI_INT, &recv_array_size);
    
        int x[recv_array_size];
        int y[recv_array_size];
        float z[recv_array_size];

        MPI_Recv(&x, recv_array_size, MPI_INT, 0, X_TAG, MPI_COMM_WORLD, &status);
        MPI_Recv(&y, recv_array_size, MPI_INT, 0, Y_TAG, MPI_COMM_WORLD, &status);

        for (int i = 0; i < recv_array_size; i++)
            z[i] = a * x[i] + b * y[i];

        MPI_Send(&z, recv_array_size, MPI_FLOAT, 0, Z_TAG, MPI_COMM_WORLD);
    }

    MPI_Finalize();
}