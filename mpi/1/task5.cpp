#include <mpi.h>
using namespace std;

/* Написать программу и запустить ее на p (= 2, 3, 4, 6, 8) процессах. На нулевом процессе объявить и задать 
массив из 12 элементов. С помощью функции MPI_Send разослать блоки массива на остальные процессы. Размер блока 
массива (12/p)+1. В результате на нулевом процессе должны быть элементы массива с 0 до 12/p, на первом 
процессе с 12/p+1 до 2×(12/p), на 3м процессе с 2×(12/p)+1 до 3×(12/p) и т.д. Вывести элементы массива на 
экран на каждом процессе. Обратите внимание, что не на все процессы хватит элементов. */

int main()
{
	MPI_Init(NULL, NULL);

	int current_process_id;
    MPI_Comm_rank(MPI_COMM_WORLD, &current_process_id);

    int number_of_processes;
    MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);

    int array_size = 12;
    int chunk_size = array_size / number_of_processes + 1;

	if (current_process_id == 0)
	{
        int array[array_size];

        for (int i = 0; i < array_size; i++)
            array[i] = rand() % 100;

        int left_items_count = array_size;

        for (int process_index = 0; process_index < number_of_processes; process_index++)
		{
            int count_for_sending;

            if (left_items_count < chunk_size)
                count_for_sending = left_items_count;
            else
                count_for_sending = chunk_size;
                
            MPI_Send(&array[array_size - left_items_count], count_for_sending, MPI_INT, process_index, 0, MPI_COMM_WORLD);
            
            left_items_count -= count_for_sending;
        }
    }

    MPI_Status status;
    MPI_Probe(0, 0, MPI_COMM_WORLD, &status);

    int new_array_size;
    MPI_Get_count(&status, MPI_INT, &new_array_size);

    int new_array[new_array_size];

    MPI_Recv(&new_array, new_array_size, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);

    string s = to_string(current_process_id) + " (" + to_string(new_array_size) + "): ";
    
	for (int i = 0; i < new_array_size; i++)
        s += to_string(new_array[i]) + " ";

    cout << s << endl;

    MPI_Finalize();
}