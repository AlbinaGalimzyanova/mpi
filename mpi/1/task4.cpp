#include <mpi.h>
using namespace std;

/* Используя блокирующую операцию передачи сообщений (MPI_Send и MPI_Recv) выполнить пересылку данных 
одномерного массива, из процесса с номером 1 на остальные процессы группы. На процессах получателях до 
выставления функции приема (MPI_Recv) определить количество принятых данных (MPI_Probe). Выделить память под 
размер приемного буфера, после чего вызвать функцию MPI_Recv. Полученное сообщение выдать на экран. */

const int SIZE = 10;

int main()
{
	MPI_Init(NULL, NULL);

	int current_process_id;
    MPI_Comm_rank(MPI_COMM_WORLD, &current_process_id);

    int number_of_processes;
    MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);
	
	if (current_process_id == 1)
	{
        int a[SIZE];

        for (int i = 0; i < SIZE; i++)
            a[i] = rand() % 100;

        for (int j = 0; j < number_of_processes; j++)
            if (j != current_process_id) 
                MPI_Send(&a, SIZE, MPI_INT, j, 0, MPI_COMM_WORLD);
    }
	else
	{
        MPI_Status status;
        MPI_Probe(1, 0, MPI_COMM_WORLD, &status);

        int new_size;
        MPI_Get_count(&status, MPI_INT, &new_size);

        int b[new_size];

        MPI_Recv(&b, new_size, MPI_INT, 1, 0, MPI_COMM_WORLD, &status);

        string s;

        for (int i = 0; i < new_size; i++)
            s += to_string(b[i]) + " ";

        cout << s << endl;
    }

    MPI_Finalize();
}