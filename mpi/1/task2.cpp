#include <iostream>
#include <mpi.h>
using namespace std;

/* Написать  программу, печатающую номер процесса и общее количество процессов в коммуникаторе 
MPI_COMM_WORLD. */

int main()
{
    MPI_Init(NULL, NULL);

    int current_process_id;
    MPI_Comm_rank(MPI_COMM_WORLD, &current_process_id);

    int number_of_processes;
    MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);

    cout << "Hello world! from thread " << current_process_id << ". Total: " << number_of_processes << endl;

    MPI_Finalize();
}