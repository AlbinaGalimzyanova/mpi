#include "mpi.h"
#include <math.h>
using namespace std;

/* Написать программу вычисления поэлементного умножения матриц Сi,j=Ai,jBi.j. Использовать функции MPI_Scatter 
для распределения элементов матриц A и B, и MPI_Gather для сбора вычисленных данных в матрицу C. */

const int COUNT = 5;

int main()
{
	MPI_Init(NULL, NULL);

	int current_process_id;
    MPI_Comm_rank(MPI_COMM_WORLD, &current_process_id);

    int number_of_processes;
    MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);

    int chunk_size = COUNT * COUNT / number_of_processes + 1;
    int *x;
    int *y;
    int *z;
    int x_local[chunk_size];
    int y_local[chunk_size];
    int z_local[chunk_size];

    if (current_process_id == 0)
    {
      x = new int[COUNT * COUNT];

        for (int i = 0; i < COUNT; i++)
            for (int j = 0; j < COUNT; j++)
                x[i * COUNT + j] = rand() % 100;  

        cout << "x:" << endl;
        
        for (int i = 0; i < COUNT; i++)
        {
            for (int j = 0; j < COUNT; j++)
                cout << x[i * COUNT + j] << " ";

            cout << endl;
        }

        y = new int[COUNT * COUNT];

        for (int i = 0; i < COUNT; i++)
            for (int j = 0; j < COUNT; j++)
                y[i * COUNT + j] = rand() % 100;

        cout << "y:" << endl;
        
        for (int i = 0; i < COUNT; i++)
        {
            for (int j = 0; j < COUNT; j++)
                cout << y[i * COUNT + j] << " ";

            cout << endl;
        }
    }

    MPI_Scatter(x, chunk_size, MPI_INT, &x_local, chunk_size, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Scatter(y, chunk_size, MPI_INT, &y_local, chunk_size, MPI_INT, 0, MPI_COMM_WORLD);
    
    for (int i = 0; i < chunk_size; i++)
    {
        z_local[i] = x_local[i] * y_local[i];
    }


    z = new int[chunk_size * chunk_size];

    for (int i = 0; i < chunk_size; i++)
        for (int j = 0; j < chunk_size; j++)
            z[i * chunk_size + j] = 0;

    MPI_Gather(z_local, chunk_size, MPI_INT, z, chunk_size, MPI_INT, 0, MPI_COMM_WORLD);

    if (current_process_id == 0)
    {
        cout << "z:" << endl;
        
        for (int i = 0; i < COUNT; i++)
        {
            for (int j = 0; j < COUNT; j++)
                cout << z[i * COUNT + j] << " ";

            cout << endl;
        }
    }

    MPI_Finalize();
}