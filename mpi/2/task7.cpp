#include "mpi.h"
#include <stdio.h>
#include <iostream>
#include <math.h>

/* Пусть векторы х, у заданы на нулевом процессе. Написать программу, в которой векторы равными блоками с 
нулевого процесса пересылаются остальным процессам. Пересылка данных осуществляется функцией MPI_Send. Все 
процессы по формуле вычисляют свои элементы искомых массивов. Каждый процесс отправляет на нулевой процесс 
подсчитанные элементы массивов. В программе реализовать следующие операции вычисления векторов:
    zi=xi+yi, где , - заданные числа;
    zi=xi*yi;
    Обмен элементов двух векторов yi xi.  */

int main()
{
    int process_number, process_count;
    MPI_Init(NULL, NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &process_count);
    MPI_Comm_rank(MPI_COMM_WORLD, &process_number);

    int a = 5;
    int b = 2;

    int arr_size = 12;
    int chunk_size = arr_size / process_count + 1;

    const int X_TAG = 0;
    const int Y_TAG = 1;
    const int Z_TAG = 2;
    const int X_RETURN_TAG = 3;
    const int Y_RETURN_TAG = 4;

    const int FORMULA_TYPE = 2;

    if (process_number == 0)
    {
        int x[arr_size];
        int y[arr_size];
        float z[arr_size];

        for (int i = 0; i < arr_size; i++)
        {
            x[i] = rand() % 100;
            y[i] = rand() * 2 % 100;
        }

        printf("x:");
        for (int i = 0; i < arr_size; i++)
            printf("%d ", x[i]);

        printf("\ny:");
        for (int i = 0; i < arr_size; i++)
            printf("%d ", y[i]);

        printf("\nz:");
        for (int i = 0; i < arr_size; i++)
            printf("%f ", z[i]);
        printf("\n");

        int left_items_count = arr_size;

        for (int process_index = 1; process_index < process_count; process_index++) 
        {
            int count_for_sending;

            if (left_items_count < chunk_size)
                count_for_sending = left_items_count;
            else
                count_for_sending = chunk_size;
            
            MPI_Send(&x[arr_size - left_items_count], count_for_sending, MPI_INT, process_index, X_TAG, MPI_COMM_WORLD);
            MPI_Send(&x[arr_size - left_items_count], count_for_sending, MPI_INT, process_index, Y_TAG, MPI_COMM_WORLD);
            
            left_items_count -= count_for_sending;
        }

        int recv_count = 0;

        for (int process_index = 1; process_index < process_count; process_index++)
        {
            MPI_Status status;
            MPI_Probe(process_index, Z_TAG, MPI_COMM_WORLD, &status);

            int recv_arr_size;
            MPI_Get_count(&status, MPI_INT, &recv_arr_size);

            MPI_Recv(&z[recv_count], recv_arr_size, MPI_FLOAT, process_index, Z_TAG, MPI_COMM_WORLD, &status);
            MPI_Recv(&x[recv_count], recv_arr_size, MPI_INT, process_index, X_RETURN_TAG, MPI_COMM_WORLD, &status);
            MPI_Recv(&y[recv_count], recv_arr_size, MPI_INT, process_index, Y_RETURN_TAG, MPI_COMM_WORLD, &status);

            recv_count += recv_arr_size;
        }

        printf("x:");
        for (int i = 0; i < arr_size; i++)
            printf("%d ", x[i]);

        printf("\ny:");
        for (int i = 0; i < arr_size; i++)
            printf("%d ", y[i]);

        printf("\nz:");
        for (int i = 0; i < arr_size; i++)
            printf("%f ", z[i]);
    }
    else
    {
        MPI_Status status;
        MPI_Probe(0, X_TAG, MPI_COMM_WORLD, &status);

        int recv_arr_size;
        MPI_Get_count(&status, MPI_INT, &recv_arr_size);

        int x[recv_arr_size];
        int y[recv_arr_size];
        float z[recv_arr_size];

        MPI_Recv(&x, recv_arr_size, MPI_INT, 0, X_TAG, MPI_COMM_WORLD, &status);
        MPI_Recv(&y, recv_arr_size, MPI_INT, 0, Y_TAG, MPI_COMM_WORLD, &status);

        for (int i = 0; i < recv_arr_size; i++)
        {
            switch (FORMULA_TYPE)
            {
                case 0:
                    z[i] = a * x[i] + b * y[i];
                    break;
                case 1:
                    z[i] = x[i] * y[i];
                    break;
                case 2:
                    int a = x[i];
                    x[i] = y[i];
                    y[i] = a;
                    break;
            }
        }

        MPI_Send(&z, recv_arr_size, MPI_FLOAT, 0, Z_TAG, MPI_COMM_WORLD);
        MPI_Send(&x, recv_arr_size, MPI_INT, 0, X_RETURN_TAG, MPI_COMM_WORLD);
        MPI_Send(&y, recv_arr_size, MPI_INT, 0, Y_RETURN_TAG, MPI_COMM_WORLD);
    }

    MPI_Finalize();
}