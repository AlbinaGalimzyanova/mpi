#include "mpi.h"
#include <stdio.h>
#include <iostream>
#include <math.h>

/* C = A * B   (C[i][j] = A[i][j] * B[i][j] ) - поэлементное умножение; */ 

int *generate_matrix(int rank, bool is_null = false, int max_value = 100) {
    int *a = new int[rank * rank];

    for (int i = 0; i < rank; i++) 
        for (int j = 0; j < rank; j++) 
            a[i * rank + j] = is_null ? 0 : rand() % max_value;

    return a;
}

void print_matrix(int *a, int rank)
{
    for (int i = 0; i < rank; i++)
    {
        for (int j = 0; j < rank; j++)
            printf("%d ", a[i * rank + j]);

        printf("\n");
    }
}

int main()
{
    int process_number, process_count;
    MPI_Init(NULL, NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &process_count);
    MPI_Comm_rank(MPI_COMM_WORLD, &process_number);

    const int RANK = 5;

    const int X_TAG = 0;
    const int Y_TAG = 1;
    const int Z_TAG = 2;
    const int X_ROW_NUMBER_TAG = 3;
    int left_items_count = RANK;

    const int CHUNK_SIZE = RANK / (process_count - 1 < RANK ? process_count - 1 : RANK) + 1;

    if (process_number == 0)
    {
        int *x = generate_matrix(RANK, false, 10);
        int *y = generate_matrix(RANK, false, 10);
        int *z = generate_matrix(RANK, true);

        print_matrix(x, RANK);
        printf("\n");
        print_matrix(y, RANK);

        int left_items_count = RANK;

        for (int process_index = 1; process_index < process_count; process_index++)
        {
            int strs_count_for_sending = left_items_count < CHUNK_SIZE ? left_items_count : CHUNK_SIZE;

            int start_index = (RANK - left_items_count) * RANK;
            int items_count_for_sending = strs_count_for_sending * RANK;

            MPI_Send(&x[start_index], items_count_for_sending, MPI_INT, process_index, X_TAG, MPI_COMM_WORLD);
            int row_number = RANK - left_items_count;
            MPI_Send(&row_number, 1, MPI_INT, process_index, X_ROW_NUMBER_TAG, MPI_COMM_WORLD);
            left_items_count -= strs_count_for_sending;

            MPI_Send(y, RANK * RANK, MPI_INT, process_index, Y_TAG, MPI_COMM_WORLD);
        }

        int recv_count = 0;

        for (int process_index = 1; process_index < process_count; process_index++)
        {
            int strs_count_for_sending = left_items_count < CHUNK_SIZE ? left_items_count : CHUNK_SIZE;

            int start_index = (RANK - left_items_count) * RANK;
            int items_count_for_sending = strs_count_for_sending * RANK;

            MPI_Send(&x[start_index], items_count_for_sending, MPI_INT, process_index, X_TAG, MPI_COMM_WORLD);
            int row_number = RANK - left_items_count;
            MPI_Send(&row_number, 1, MPI_INT, process_index, X_ROW_NUMBER_TAG, MPI_COMM_WORLD);
            left_items_count -= strs_count_for_sending;

            MPI_Status status;
            MPI_Probe(process_index, Z_TAG, MPI_COMM_WORLD, &status);

            int recv_arr_size;
            MPI_Get_count(&status, MPI_INT, &recv_arr_size);
            MPI_Recv(&z[recv_count], recv_arr_size, MPI_INT, process_index, Z_TAG, MPI_COMM_WORLD, &status);

            recv_count += recv_arr_size;
        }
        printf("\n");
        print_matrix(z, RANK);
    }
    else
    {
        MPI_Status status;
        MPI_Probe(0, X_TAG, MPI_COMM_WORLD, &status);

        int recv_arr_size;
        MPI_Get_count(&status, MPI_INT, &recv_arr_size);

        int x[recv_arr_size];
        int y[RANK * RANK];
        int z[recv_arr_size];

        int row_number;

        MPI_Recv(x, recv_arr_size, MPI_INT, 0, X_TAG, MPI_COMM_WORLD, &status);
        MPI_Recv(&row_number, 1, MPI_INT, 0, X_ROW_NUMBER_TAG, MPI_COMM_WORLD, &status);
        MPI_Recv(y, RANK * RANK, MPI_INT, 0, Y_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        for (int local_row_index = 0; local_row_index < recv_arr_size / RANK; local_row_index++) 
        {
            for (int j = 0; j < RANK; j++)
            {
                z[local_row_index * RANK + j] = 0;
                for (int k = 0; k < RANK; k++)
                {
                    int count_for_sending;
                    if (left_items_count < CHUNK_SIZE)
                        count_for_sending = left_items_count;
                    else
                        count_for_sending = CHUNK_SIZE;  z[local_row_index * RANK + j] += x[local_row_index * RANK + k] * y[row_number + k * RANK + j];
                }
            }
        }

        MPI_Send(&z, recv_arr_size, MPI_INT, 0, Z_TAG, MPI_COMM_WORLD);
    }

    MPI_Finalize();
}