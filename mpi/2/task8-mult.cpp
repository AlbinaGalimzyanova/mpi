#include "mpi.h"
#include <stdio.h>
#include <iostream>
#include <math.h>

/* Пусть матрицы вещественных чисел A, B заданы на нулевом процессе. Написать программу, в которой матрицы 
равными кусками с нулевого процесса пересылаются остальным процессам. Пересылка данных осуществляется функцией 
MPI_Send. Все процессы по формуле вычисляют свои элементы искомых матриц. Каждый процесс отправляет на нулевой 
процесс подсчитанные элементы матриц. В программе реализовать следующие операции вычисления матриц:
    C = A * B   (C[i][j] = A[i][j] * B[i][j] ) - поэлементное умножение;
    C = A * B   -  умножение матриц;
    B = AT       транспонирование матрицы;
    (*) T = gauss(A)    - приведение матрицы к нижнему треугольному виду методом исключения Гаусса */
    
int *generate_matrix(int rank, bool is_null = false, int max_value = 100) {
    int *a = new int[rank * rank];

    for (int i = 0; i < rank; i++) {
        for (int j = 0; j < rank; j++) {
            a[i * rank + j] = is_null ? 0 : rand() % max_value;
        }
    }
    return a;
}

void print_matrix(int *a, int rank) {
    for (int i = 0; i < rank; i++) {
        for (int j = 0; j < rank; j++) {
            printf("%d ", a[i * rank + j]);
        }
        printf("\n");
    }
}

int main() {
    int process_number, process_count;
    MPI_Init(NULL, NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &process_count);
    MPI_Comm_rank(MPI_COMM_WORLD, &process_number);

    const int RANK = 5;

    const int X_TAG = 0;
    const int Y_TAG = 1;
    const int Z_TAG = 2;

    const int CHUNK_SIZE = process_count - 1 > RANK ? process_count - 1 : RANK + 1;

    if (process_number == 0) {
        int *x = generate_matrix(RANK);
        int *y = generate_matrix(RANK);
        int *z = generate_matrix(RANK, true);

        print_matrix(x, RANK);
        printf("\n");
        print_matrix(y, RANK);

        int left_items_count = RANK;

        for (int process_index = 1; process_index < process_count; process_index++) {
            int strs_count_for_sending = left_items_count < CHUNK_SIZE ? left_items_count : CHUNK_SIZE;

            int start_index = (RANK - left_items_count) * RANK;
            int items_count_for_sending = strs_count_for_sending * RANK;

            MPI_Send(&x[start_index], items_count_for_sending, MPI_INT, process_index, X_TAG, MPI_COMM_WORLD);
            MPI_Send(&y[start_index], items_count_for_sending, MPI_INT, process_index, Y_TAG, MPI_COMM_WORLD);
            left_items_count -= strs_count_for_sending;
        }

        int recv_count = 0;

        for (int process_index = 1; process_index < process_count; process_index++) {
            MPI_Status status;
            MPI_Probe(process_index, Z_TAG, MPI_COMM_WORLD, &status);

            int recv_arr_size;
            MPI_Get_count(&status, MPI_INT, &recv_arr_size);
            MPI_Recv(&z[recv_count * RANK], recv_arr_size, MPI_INT, process_index, Z_TAG, MPI_COMM_WORLD, &status);

            recv_count += recv_arr_size / RANK;
        }
        printf("\n");
        print_matrix(z, RANK);
    } else {
        MPI_Status status;
        MPI_Probe(0, X_TAG, MPI_COMM_WORLD, &status);

        int recv_arr_size;
        MPI_Get_count(&status, MPI_INT, &recv_arr_size);

        int x[recv_arr_size];
        int y[recv_arr_size];
        int z[recv_arr_size];

        MPI_Recv(&x, recv_arr_size, MPI_INT, 0, X_TAG, MPI_COMM_WORLD, &status);
        MPI_Recv(&y, recv_arr_size, MPI_INT, 0, Y_TAG, MPI_COMM_WORLD, &status);

        for (int i = 0; i < recv_arr_size; i++) {
            z[i] = x[i] * y[i];
        }

        MPI_Send(&z, recv_arr_size, MPI_FLOAT, 0, Z_TAG, MPI_COMM_WORLD);
    }

    MPI_Finalize();
}